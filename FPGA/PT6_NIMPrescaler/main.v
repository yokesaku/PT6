`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Kyoto University
// Engineer: Takuto KUNIGO
// 
// Create Date:    12/02/2015
// Design Name: 
// Module Name:    NIM Prescaler
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module NIMPrescaler
(
	output	wire				EXT_OUT,
	input		wire				FPGA_GCLK,
	input		wire				ALL_RESET_B
);

parameter	[3:0]	fact	=	4'd10;

Prescaler	Prescaler(.INPUT(FPGA_GCLK), .RESET(ALL_RESET_B), .FACTOR(fact), .OUTPUT(EXT_OUT) );





endmodule // end of NIMPrescaler