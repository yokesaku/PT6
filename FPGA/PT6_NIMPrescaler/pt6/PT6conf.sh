#/bin/sh

export bit_TX=FPGA/PT6_glTXTX/gltxtx.bit
export bit_RX=FPGA/PT6_gl1TCPv2/gl1tcp.bit

echo "erase TX PT6"
src/pt6 e 0x0f1
src/pt6 e 0x0f2
src/pt6 e 0x0f3
src/pt6 e 0x0f4
src/pt6 e 0x0f5
src/pt6 e 0x0f6
src/pt6 e 0x0f7
src/pt6 e 0x0f8
src/pt6 e 0x0f9
src/pt6 e 0x0fa
echo ""

echo "erase RX PT6"
src/pt6 e 0x0eb
src/pt6 e 0x0ec
src/pt6 e 0x0ed
src/pt6 e 0x0ee
src/pt6 e 0x0ef

echo "all FPGA was eraced"
echo ""
echo ""
echo ""

echo "configure TX PT6"
src/pt6 c $bit_TX 0x0f1
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f100018 0x8000
src/pt6 c $bit_TX 0x0f2
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f200018 0x8000
src/pt6 c $bit_TX 0x0f3
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f300018 0x8000
src/pt6 c $bit_TX 0x0f4
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f400018 0x8000
src/pt6 c $bit_TX 0x0f5
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f500018 0x8000
src/pt6 c $bit_TX 0x0f6
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f600018 0x8000
src/pt6 c $bit_TX 0x0f7
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f700018 0x8000
src/pt6 c $bit_TX 0x0f8
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f800018 0x8000
src/pt6 c $bit_TX 0x0f9
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0f900018 0x8000
src/pt6 c $bit_TX 0x0fa
/local/vmedrv/ver121/vmeput /dev/vmedrv32d32 0x0fa00018 0x8000
echo ""

echo "configure RX PT6"
src/pt6 c $bit_RX 0x0eb
src/pt6 c $bit_RX 0x0ec
src/pt6 c $bit_RX 0x0ed
src/pt6 c $bit_RX 0x0ee
src/pt6 c $bit_RX 0x0ef
echo "all FPGA was configured"
