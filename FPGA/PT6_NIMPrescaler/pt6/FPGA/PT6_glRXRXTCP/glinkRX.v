`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    05:26:54 08/21/2015 
// Design Name: 
// Module Name:    glinkRX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module glinkRX(
    input 	wire				RXCLK1,			// Recovered Word-Rate Clock Outputs
	 input	wire				RESET,			//	Reset

    input	wire	[15:0] 	RX,				// Word Outputs
    input	wire				RXFLAG,			// Flag Bit, additional bit

    input	wire	 			RXDATA,			// Data Word Available Output
    input	wire 				RXCNTL,			// Control Word Available Output

	 input	wire				RXSD,				// Signal Detect
    input	wire				RXREADY,			// Receiver Ready
    input	wire				RXERROR,			// Receiver Data Error
	 input	wire				RXDSLIP,			// Rx Word Slip
	 
	 output	wire				SHFIN,			// Shift Input
	 input	wire				SHFOUT,			// Shift Output
	 
	 output	wire				SRQIN,			// Shift Request Input

	 input	wire				in_rxflgenb,
	 input	wire				in_esmpxenb,
	 input	wire				in_passenb,
	 input	wire	[1:0]		in_div,

    output	reg		 		out_rxflgenb, // Flag Bit Mode Select
	 output	reg				out_esmpxenb, 	// Enhanced Simplex Mode Enable (the Tx chip on a SL board, this pin is set to low)
	 output	reg				out_passenb,	// Enable Parallel Automatic Synchronization System
    output	reg	[1:0] 	out_div,			// VCO Divider Select
	 
	 output	wire				TSTCLK,			// External Serial Rate Clock Input
	 output	wire				WSYNCDSB,			// Word Sync Disable
	 
	 output	reg	[15:0]	data,
	 output	reg				flag,
	 output	reg	[1:0]		glink_mode,
	 output	reg	[3:0]		glink_status
    );

assign SRQIN	=	1'b0;
assign SHFIN	=	SHFOUT;

assign WSYNCDSB	= 1'b0;
assign TSTCLK		= 1'b1;

initial
	begin
		out_div			<=	2'b01;
		out_rxflgenb	<=	1'b0;
		out_esmpxenb	<=	1'b0;
		out_passenb		<=	1'b1;
	end

always @(negedge RXCLK1 or negedge RESET)
	begin
		if(!RESET | !RXSD)
			begin
				out_div			<=	2'b01;
				out_rxflgenb	<=	1'b0;
				out_esmpxenb	<=	1'b0;
				out_passenb		<=	1'b1;
			end
		else
			begin
				out_div			<=	in_div;
				out_rxflgenb	<=	in_rxflgenb;
				out_esmpxenb	<=	in_esmpxenb;
				out_passenb		<=	in_passenb;

				data 				<= RX;
				flag				<=	RXFLAG;
				glink_mode 		<= {RXCNTL, RXDATA};
				glink_status	<=	{RXSD, RXREADY, RXERROR, RXDSLIP};
			end
	end

endmodule
