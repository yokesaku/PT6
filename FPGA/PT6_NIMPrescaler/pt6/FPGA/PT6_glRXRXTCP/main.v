`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ICEPP
// Engineer: Ikuo Otani
// 
// Create Date:    9/28/2012 
// Design Name: 
// Module Name:    glink mouth1 to TCP 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module glRXRXTCP
(
	input		wire				FPGA_GCLK,
	input		wire				ALL_RESET_B,

	output	wire				FPGA_STATE,
	input		wire				FPGA_SELECT,

	// VME
	input		wire				FPGA_WSTR_B,
	input		wire				FPGA_RSTR_B,
	input		wire	[7:0]		VME_A,
	inout		wire	[15:0]	VME_D,


	input		wire			TILE0_GTP0_REFCLK_PAD_N_IN,
	input		wire			TILE0_GTP0_REFCLK_PAD_P_IN,
	input		wire	[3:0]	RXN_IN,
	input		wire	[3:0]	RXP_IN,
	output	wire	[3:0]	TXN_OUT,
	output	wire	[3:0]	TXP_OUT,

	// Ethernet
	output	wire			ETH_RSTn,
	input		wire			ETH_CRS,
	input		wire			ETH_COL,
	output	wire			ETH_GTXCLK,
	input		wire			ETH_TX_CLK,
	output	wire	[7:0]	ETH_TX_D,
	output	wire			ETH_TX_EN,
	output	wire			ETH_TX_ER,
	input		wire			ETH_RX_CLK,
	input		wire	[7:0]	ETH_RX_D,
	input		wire			ETH_RX_DV,
	input		wire			ETH_RX_ER,

	output	wire			ETH_MACCLK,
	//input		wire		ETH_INTn,
	output	wire			ETH_MDC,
	inout		wire			ETH_MDIO,

	output	wire			PROM_CS,
	output	wire			PROM_SK,
	output	wire			PROM_DI,
	input		wire			PROM_DO,

	input		wire			LED_1000M,

	input		wire	[15:0]	RX_1,
	input		wire				RXCLK1_1,
	input		wire				RXFLAG_1,
	input		wire				RXDATA_1,
	input		wire				RXCNTL_1,
	input		wire				RXREADY_1,
	input		wire				RXERROR_1,
	input		wire				RXDSLIP_1,
	output	wire	[1:0]		RXDIV_1,
	output	wire				RXFLGENB_1,
	output	wire				RXESMPXENB_1,
	output	wire				RXPASSENB_1,
	input		wire				RXSD_1,
	output	wire				RXWSYNCDSB_1,
	output	wire				RXTSTCLK_1,
	input		wire				RXSHFOUT_1,
	output	wire				RXSHFIN_1,
	output	wire				RXSRQIN_1,
	
	input		wire	[15:0]	RX_2,
	input		wire				RXCLK1_2,
	input		wire				RXFLAG_2,
	input		wire				RXDATA_2,
	input		wire				RXCNTL_2,
	input		wire				RXREADY_2,
	input		wire				RXERROR_2,
	input		wire				RXDSLIP_2,
	output	wire	[1:0]		RXDIV_2,
	output	wire				RXFLGENB_2,
	output	wire				RXESMPXENB_2,
	output	wire				RXPASSENB_2,
	input		wire				RXSD_2,
	output	wire				RXWSYNCDSB_2,
	output	wire				RXTSTCLK_2,
	input		wire				RXSHFOUT_2,
	output	wire				RXSHFIN_2,
	output	wire				RXSRQIN_2
);

assign FPGA_STATE =  RXSD_1 && RXREADY_1 && !RXERROR_1;




// G-Link
wire	[15:0]	rx_1_data, rx_2_data;
wire				rx_1_flag, rx_2_flag;
wire	[1:0]		rx_1_mode, rx_2_mode;
wire	[3:0]		rx_1_status, rx_2_status;
wire	[6:0]		glink_set_1, glink_set_2;

glinkRX RX_module_1(	.RXCLK1(RXCLK1_1),	.RESET(ALL_RESET_B),
							.RX(RX_1),	.RXFLAG(RXFLAG_1),	.RXDATA(RXDATA_1),	.RXCNTL(RXCNTL_1),
							.RXSD(RXSD_1),	.RXREADY(RXREADY_1),	.RXERROR(RXERROR_1),	.RXDSLIP(RXDSLIP_1),
							.SHFIN(RXSHFIN_1),	.SHFOUT(RXSHFOUT_1),	.SRQIN(RXSRQIN_1),
							.in_rxflgenb(glink_set_1[6]), .in_esmpxenb(glink_set_1[5]),	.in_passenb(glink_set_1[4]),	.in_div(glink_set_1[1:0]),
							.out_rxflgenb(RXFLGENB_1),	.out_esmpxenb(RXESMPXENB_1),	.out_passenb(RXPASSENB_1),	.out_div(RXDIV_1),
							.data(rx_1_data),	.flag(rx_1_flag),	.glink_mode(rx_1_mode),	.glink_status(rx_1_status) );
glinkRX RX_module_2(	.RXCLK1(RXCLK1_2),	.RESET(ALL_RESET_B),
							.RX(RX_2),	.RXFLAG(RXFLAG_2),	.RXDATA(RXDATA_2),	.RXCNTL(RXCNTL_2),
							.RXSD(RXSD_2),	.RXREADY(RXREADY_2),	.RXERROR(RXERROR_2),	.RXDSLIP(RXDSLIP_2),
							.SHFIN(RXSHFIN_2),	.SHFOUT(RXSHFOUT_2),	.SRQIN(RXSRQIN_2),
							.in_rxflgenb(glink_set_2[6]), .in_esmpxenb(glink_set_2[5]),	.in_passenb(glink_set_2[4]),	.in_div(glink_set_2[1:0]),
							.out_rxflgenb(RXFLGENB_2),	.out_esmpxenb(RXESMPXENB_2),	.out_passenb(RXPASSENB_2),	.out_div(RXDIV_2),
							.data(rx_2_data),	.flag(rx_2_flag),	.glink_mode(rx_2_mode),	.glink_status(rx_2_status) );




//**************************** Wire Declarations ******************************
    wire            tile0_txusrclk0_i;
    wire            tile0_gtpclkout0_0_to_bufg_i;



//------------------------------------------------------------------------------
//	definition of clock & Reset
//------------------------------------------------------------------------------
	wire			sysDcmLocked		;
	wire			SYSCLK				;
	wire			Dcm25Locked			;
	wire			RST					;
	wire			ETH_1000M			;

   	sysdcm			SYSDCM(
	    .CLK_IN1		(FPGA_GCLK),
		.RESET			(!ALL_RESET_B	),
		.CLK_OUT1		(SYSCLK			),	// 130MHz
		.LOCKED			(sysDcmLocked	)
	);

   	dcm25			DCM25(
		.CLK_IN1		(tile0_txusrclk0_i),
		.RESET			(!ALL_RESET_B	),
		.CLK_OUT1		(ETH_MACCLK		), // 25MHz
		.LOCKED			(Dcm25Locked	)
	);

   assign   RST   = !sysDcmLocked || !Dcm25Locked || !ALL_RESET_B;
	
	assign	ETH_1000M = !LED_1000M;

	BUFGMUX GMIIMUX(.O(int_ETH_TX_CLK), .I0(ETH_TX_CLK), .I1(tile0_txusrclk0_i), .S(ETH_1000M));

	ODDR2	IOB_GTX		(.Q(ETH_GTXCLK), .C0(tile0_txusrclk0_i), .C1(~tile0_txusrclk0_i), .CE(1'b1), .D0(1'b1), .D1(1'b0), .R(1'b0), .S(1'b0));

//------------------------------------------------------------------------------
//	NETWORK PROTOCOL PROCESSOR (body of SiTCP)
//------------------------------------------------------------------------------
	wire	[15:0]	TCP_RX_WC			;
	wire	[7:0]	TCP_RX_DATA			;
	wire	[7:0]	TCP_TX_DATA			;

	wire	[31:0]	RBCP_ADDR			;
	wire	[7:0]	RBCP_WD				;
	wire	[7:0]	RBCP_RD				;
	
	assign	ETH_MDIO	= (ETH_MDIO_OE	? ETH_MDIO_OUT	: 1'bz);

	WRAP_SiTCP_GMII_XC6S_16K		
		#(130) // = System clock frequency(MHz), integer only
	SiTCP(
		.CLK					(SYSCLK				),	// in	: System Clock > 129MHz
		.RST					(RST				),	// in	: System reset
	// Configuration parameters
		.FORCE_DEFAULTn			(1'b0				),	// in	: Load default parameters
		.EXT_IP_ADDR			(32'd0	),	// in	: IP address[31:0]
		.EXT_TCP_PORT			(16'd0				),	// in	: TCP port #[15:0]
		.EXT_RBCP_PORT			(16'd0			),	// in	: RBCP port #[15:0]
		.PHY_ADDR				(5'b00001			),	// in	: PHY-device MIF address[4:0]
	// EEPROM
		.EEPROM_CS				(PROM_CS),	// out	: Chip select
		.EEPROM_SK				(PROM_SK),	// out	: Serial data clock
		.EEPROM_DI				(PROM_DI),	// out	: Serial write data
		.EEPROM_DO				(PROM_DO),	// in	: Serial read data
	// MII interface
		.GMII_RSTn				(ETH_RSTn			),	// out	: PHY reset
		.GMII_1000M				(ETH_1000M			),	// in	: GMII mode (0:MII, 1:GMII)
		// TX
		.GMII_TX_CLK			(int_ETH_TX_CLK	),	// in	: Tx clock
		.GMII_TX_EN				(ETH_TX_EN			),	// out	: Tx enable
		.GMII_TXD				(ETH_TX_D[7:0]		),	// out	: Tx data[7:0]
		.GMII_TX_ER				(ETH_TX_ER			),	// out	: TX error
		// RX
		.GMII_RX_CLK			(ETH_RX_CLK		),	// in	: Rx clock
		.GMII_RX_DV				(ETH_RX_DV			),	// in	: Rx data valid
		.GMII_RXD				(ETH_RX_D[7:0]		),	// in	: Rx data[7:0]
		.GMII_RX_ER				(ETH_RX_ER			),	// in	: Rx error
		.GMII_CRS				(ETH_CRS			),	// in	: Carrier sense
		.GMII_COL				(ETH_COL			),	// in	: Collision detected
		// Management IF
		.GMII_MDC				(ETH_MDC			),	// out	: Clock for MDIO
		.GMII_MDIO_IN			(ETH_MDIO			),	// in	: Data
		.GMII_MDIO_OUT			(ETH_MDIO_OUT		),	// out	: Data
		.GMII_MDIO_OE			(ETH_MDIO_OE		),	// out	: MDIO output enable
	// User I/F
		.SiTCP_RST				(SiTCP_RST			),	// out	: Reset for SiTCP and related circuits
		// TCP connection control
		.TCP_OPEN_REQ			(1'b0				),	// in	: Reserved input, shoud be 0
		.TCP_OPEN_ACK			(TCP_OPEN			),	// out	: Acknowledge for open (=Socket busy)
		.TCP_ERROR				(),	// out	: TCP error, its active period is equal to MSL
		.TCP_CLOSE_REQ			(TCP_CLOSE			),	// out	: Connection close request
		.TCP_CLOSE_ACK			(TCP_CLOSE			),	// in	: Acknowledge for closing
		// FIFO I/F
		.TCP_RX_WC				(TCP_RX_WC[15:0]	),	// in	: Rx FIFO write count[15:0] (Unused bits should be set 1)
		.TCP_RX_WR				(TCP_RX_WR			),	// out	: Write enable
		.TCP_RX_DATA			(TCP_RX_DATA[7:0]	),	// out	: Write data[7:0]
		.TCP_TX_FULL			(TCP_TX_FULL		),	// out	: Almost full flag
		.TCP_TX_WR				(TCP_TX_WR			),	// in	: Write enable
		.TCP_TX_DATA			(TCP_TX_DATA[7:0]	),	// in	: Write data[7:0]
		// RBCP
//		.RBCP_ACT				(RBCP_ACT			),	// out	: RBCP active
		.RBCP_ADDR				(RBCP_ADDR[31:0]	),	// out	: Address[31:0]
		.RBCP_WD				(RBCP_WD[7:0]		),	// out	: Data[7:0]
		.RBCP_WE				(RBCP_WE			),	// out	: Write enable
		.RBCP_RE				(RBCP_RE			),	// out	: Read enable
		.RBCP_ACK				(RBCP_ACK			),	// in	: Access acknowledge
		.RBCP_RD				(RBCP_RD[7:0]		)	// in	: Read data[7:0]
	);

//------------------------------------------------------------------------------
//	TCP loopback FIFO (not used but cannot be removed)
//------------------------------------------------------------------------------

	assign	TCP_RX_WC[15:11]	= 5'b11111;

	sitcp_fifo		roopback_fifo(
		.clk			(SYSCLK				),
		.rst			(TCP_OPEN			),
		.data_count		(TCP_RX_WC[10:0]	),
		.full			(),
		.wr_en			(TCP_RX_WR			),
		.din			(TCP_RX_DATA[7:0]	),
		.empty			(sitcpFifoEmpty		),
		.rd_en			(sitcpFifoRe		),
		.dout			(/*TCP_TX_DATA[7:0]*/	),
		.valid			(/*TCP_TX_WR*/			)
	);

	assign	sitcpFifoRe	= ~TCP_TX_FULL & ~sitcpFifoEmpty;

//------------------------------------------------------------------------------
//	RBCP Registers (free registers 0x8-0xf)
//------------------------------------------------------------------------------
	RBCP_REG		RBCP_REG(
		// System
		.CLK					(SYSCLK				),	// in	: System clock
		.RST					(SiTCP_RST			),	// in	: System reset
//		.LED					(		),	// out	: LED[2:0]
//		.DIP_SW					(4'b0101		),	// in	: DIP_SW[3:0]
		// RBCP I/F
//		.RBCP_ACT				(RBCP_ACT			),	// in	: Active
		.RBCP_ADDR				(RBCP_ADDR[31:0]	),	// in	: Address[31:0]
		.RBCP_WE				(RBCP_WE			),	// in	: Write enable
		.RBCP_WD				(RBCP_WD[7:0]		),	// in	: Write data[7:0]
		.RBCP_RE				(RBCP_RE			),	// in	: Read enable
		.RBCP_RD				(RBCP_RD[7:0]		),	// out	: Read data[7:0]
		.RBCP_ACK				(RBCP_ACK			)	// out	: Acknowledge
	);



//------------------------------------------------------------------------------
//	fifo between glink and SiTCP
//------------------------------------------------------------------------------

sitcpfifo sitcpfifo(
  .rst(!ALL_RESET_B), // input rst
  .wr_clk(~RXCLK1_1), // input wr_clk
  .rd_clk(SYSCLK), // input rd_clk
  .din(glinkrx), // input [15 : 0] din
  .wr_en(glinkrx_flg!=2'b00 && TCP_OPEN), // input wr_en
  .rd_en(!TCP_TX_FULL), // input rd_en
  .dout(TCP_TX_DATA), // output [15 : 0] dout
//  .full(full), // output full
//  .wr_ack(wr_ack), // output wr_ack
//  .empty(empty), // output empty
  .valid(TCP_TX_WR) // output valid
);


//************************** Register Declarations ****************************

//**************************** Wire Declarations ******************************

    //------------------------ MGT Wrapper Wires ------------------------------

    //________________________________________________________________________
    //________________________________________________________________________
    //TILE0   (X0_Y1)

    //------------------------------- PLL Ports --------------------------------
    wire            tile0_gtpreset0_i;
    wire            tile0_gtpreset1_i;
    wire            tile0_plllkdet1_i;
    wire            tile0_resetdone0_i;
    wire            tile0_resetdone1_i;
    //--------------------- Receive Ports - 8b10b Decoder ----------------------
    wire            tile0_rxdisperr0_i;
    wire            tile0_rxdisperr1_i;
    wire            tile0_rxnotintable0_i;
    wire            tile0_rxnotintable1_i;
    //------------- Receive Ports - Comma Detection and Alignment --------------
    wire            tile0_rxenmcommaalign0_i = 1'b1;
    wire            tile0_rxenmcommaalign1_i = 1'b1;
    wire            tile0_rxenpcommaalign0_i = 1'b1;
    wire            tile0_rxenpcommaalign1_i = 1'b1;
    //----------------- Receive Ports - RX Data Path interface -----------------
    wire    [7:0]   tile0_rxdata0_i;
    wire    [7:0]   tile0_rxdata1_i;
    //------------- Receive Ports - RX Loss-of-sync State Machine --------------
    wire    [1:0]   tile0_rxlossofsync0_i;
    wire    [1:0]   tile0_rxlossofsync1_i;
    //-------------------------- TX/RX Datapath Ports --------------------------
    wire    [1:0]   tile0_gtpclkout0_i;
    wire    [1:0]   tile0_gtpclkout1_i;
    //----------------- Transmit Ports - 8b10b Encoder Control -----------------
    reg            tile0_txcharisk0_i;
    reg            tile0_txcharisk1_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [7:0]   tile0_txdata0_i;
    wire    [7:0]   tile0_txdata1_i;

    //________________________________________________________________________
    //________________________________________________________________________
    //TILE1   (X1_Y1)

    //------------------------------- PLL Ports --------------------------------
    wire            tile1_gtpreset0_i;
    wire            tile1_gtpreset1_i;
    wire            tile1_plllkdet0_i;
    wire            tile1_plllkdet1_i;
    wire            tile1_resetdone0_i;
    wire            tile1_resetdone1_i;
    //--------------------- Receive Ports - 8b10b Decoder ----------------------
    wire            tile1_rxdisperr0_i;
    wire            tile1_rxdisperr1_i;
    wire            tile1_rxnotintable0_i;
    wire            tile1_rxnotintable1_i;
    //------------- Receive Ports - Comma Detection and Alignment --------------
    wire            tile1_rxenmcommaalign0_i = 1'b1;
    wire            tile1_rxenmcommaalign1_i = 1'b1;
    wire            tile1_rxenpcommaalign0_i = 1'b1;
    wire            tile1_rxenpcommaalign1_i = 1'b1;
    //----------------- Receive Ports - RX Data Path interface -----------------
    wire    [7:0]   tile1_rxdata0_i;
    wire    [7:0]   tile1_rxdata1_i;
    //------------- Receive Ports - RX Loss-of-sync State Machine --------------
    wire    [1:0]   tile1_rxlossofsync0_i;
    wire    [1:0]   tile1_rxlossofsync1_i;
    //-------------------------- TX/RX Datapath Ports --------------------------
    wire    [1:0]   tile1_gtpclkout0_i;
    wire    [1:0]   tile1_gtpclkout1_i;
    //----------------- Transmit Ports - 8b10b Encoder Control -----------------
    reg            tile1_txcharisk0_i;
    reg            tile1_txcharisk1_i;
    //---------------- Transmit Ports - TX Data Path interface -----------------
    wire    [7:0]   tile1_txdata0_i;
    wire    [7:0]   tile1_txdata1_i;

//**************************** Main Body of Code *******************************


reg [7:0] transdata = 8'b1011_1100;

always@(posedge tile0_txusrclk0_i or negedge ALL_RESET_B)begin 
	if (!ALL_RESET_B) begin
		transdata <= 8'b1011_1100;
		tile0_txcharisk0_i <= 1'b1;
		tile0_txcharisk1_i <= 1'b1;
		tile1_txcharisk0_i <= 1'b1;
		tile1_txcharisk1_i <= 1'b1;
	end
	else begin
		transdata <= VME_A[7:0];
		tile0_txcharisk0_i <= 1'b0;
		tile0_txcharisk1_i <= 1'b0;
		tile1_txcharisk0_i <= 1'b0;
		tile1_txcharisk1_i <= 1'b0;
	end
end

assign tile0_txdata0_i = transdata;
assign tile0_txdata1_i = transdata;
assign tile1_txdata0_i = transdata;
assign tile1_txdata1_i = transdata;


    //---------------------Dedicated GTP Reference Clock Inputs ---------------
    // The dedicated reference clock inputs you selected in the GUI are implemented using
    // IBUFDS instances.
    //
    // In the UCF file for this example design, you will see that each of
    // these IBUFDS instances has been LOCed to a particular set of pins. By LOCing to these
    // locations, we tell the tools to use the dedicated input buffers to the GTP reference
    // clock network, rather than general purpose IOs. To select other pins, consult the 
    // Implementation chapter of UG___, or rerun the wizard.
    //
    // This network is the highest performace (lowest jitter) option for providing clocks
    // to the GTP transceivers.
    
    IBUFDS tile0_gtp0_refclk_ibufds_i
    (
        .O                              (tile0_gtp0_refclk_i),
        .I                              (TILE0_GTP0_REFCLK_PAD_P_IN),
        .IB                             (TILE0_GTP0_REFCLK_PAD_N_IN)
    );

    //--------------------------------- User Clocks ---------------------------
    
    // The clock resources in this section were added based on userclk source selections on
    // the Latency, Buffering, and Clocking page of the GUI. A few notes about user clocks:
    // * The userclk and userclk2 for each GTP datapath (TX and RX) must be phase aligned to 
    //   avoid data errors in the fabric interface whenever the datapath is wider than 10 bits
    // * To minimize clock resources, you can share clocks between GTPs. GTPs using the same frequency
    //   or multiples of the same frequency can be accomadated using DCMs and PLLs. Use caution when
    //   using RXRECCLK as a clock source, however - these clocks can typically only be shared if all
    //   the channels using the clock are receiving data from TX channels that share a reference clock 
    //   source with each other.

    BUFIO2 #
    (
        .DIVIDE                         (1),
        .DIVIDE_BYPASS                  ("TRUE")
    )
    gtpclkout0_0_bufg0_bufio2_i
    (
        .I                              (tile0_gtpclkout0_i[0]),
        .DIVCLK                         (tile0_gtpclkout0_0_to_bufg_i),
        .IOCLK                          (),
        .SERDESSTROBE                   ()
    );

    BUFG gtpclkout0_0_bufg0_i
    (
        .I                              (tile0_gtpclkout0_0_to_bufg_i),
        .O                              (tile0_txusrclk0_i)
    );



    //--------------------------- The GTP Wrapper -----------------------------
    
    // Use the instantiation template in the examples directory to add the GTP wrapper to your design.
    // In this example, the wrapper is wired up for basic operation with a frame generator and frame 
    // checker. The GTPs will reset, then attempt to align and transmit data. If channel bonding is 
    // enabled, bonding should occur after alignment.

    S6_GTPWIZARD_V1_7 #
    (
        .WRAPPER_SIM_GTPRESET_SPEEDUP           (1),
        .WRAPPER_CLK25_DIVIDER_0                (5),
        .WRAPPER_CLK25_DIVIDER_1                (5),
        .WRAPPER_PLL_DIVSEL_FB_0                (2),
        .WRAPPER_PLL_DIVSEL_FB_1                (2),
        .WRAPPER_PLL_DIVSEL_REF_0               (1),
        .WRAPPER_PLL_DIVSEL_REF_1               (1),
        .WRAPPER_SIMULATION                     (0)
    )
    s6_gtpwizard_v1_7_i
    (
 
        //_____________________________________________________________________
        //_____________________________________________________________________
        //TILE0  (X0_Y1)

        //---------------------- Loopback and Powerdown Ports ----------------------
        .TILE0_LOOPBACK0_IN             (3'b000),
        .TILE0_LOOPBACK1_IN             (3'b000),
        //------------------------------- PLL Ports --------------------------------
        .TILE0_CLK00_IN                 (tile0_gtp0_refclk_i),
        .TILE0_CLK01_IN                 (tile0_gtp0_refclk_i),
        .TILE0_GTPRESET0_IN             (tile0_gtpreset0_i),
        .TILE0_GTPRESET1_IN             (tile0_gtpreset1_i),
        .TILE0_PLLLKDET1_OUT            (tile0_plllkdet1_i),
        .TILE0_RESETDONE0_OUT           (tile0_resetdone0_i),
        .TILE0_RESETDONE1_OUT           (tile0_resetdone1_i),
        //--------------------- Receive Ports - 8b10b Decoder ----------------------
        .TILE0_RXDISPERR0_OUT           (tile0_rxdisperr0_i),
        .TILE0_RXDISPERR1_OUT           (tile0_rxdisperr1_i),
        .TILE0_RXNOTINTABLE0_OUT        (tile0_rxnotintable0_i),
        .TILE0_RXNOTINTABLE1_OUT        (tile0_rxnotintable1_i),
        //------------- Receive Ports - Comma Detection and Alignment --------------
        .TILE0_RXENMCOMMAALIGN0_IN      (tile0_rxenmcommaalign0_i),
        .TILE0_RXENMCOMMAALIGN1_IN      (tile0_rxenmcommaalign1_i),
        .TILE0_RXENPCOMMAALIGN0_IN      (tile0_rxenpcommaalign0_i),
        .TILE0_RXENPCOMMAALIGN1_IN      (tile0_rxenpcommaalign1_i),
        //----------------- Receive Ports - RX Data Path interface -----------------
        .TILE0_RXDATA0_OUT              (tile0_rxdata0_i),
        .TILE0_RXDATA1_OUT              (tile0_rxdata1_i),
        .TILE0_RXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE0_RXUSRCLK21_IN            (tile0_txusrclk0_i),
        //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        .TILE0_RXEQMIX0_IN              (2'b00),
        .TILE0_RXEQMIX1_IN              (2'b00),
        .TILE0_RXN0_IN                  (RXN_IN[0]),
        .TILE0_RXN1_IN                  (RXN_IN[1]),
        .TILE0_RXP0_IN                  (RXP_IN[0]),
        .TILE0_RXP1_IN                  (RXP_IN[1]),
        //------------- Receive Ports - RX Loss-of-sync State Machine --------------
        .TILE0_RXLOSSOFSYNC0_OUT        (tile0_rxlossofsync0_i),
        .TILE0_RXLOSSOFSYNC1_OUT        (tile0_rxlossofsync1_i),
        //-------------------------- TX/RX Datapath Ports --------------------------
        .TILE0_GTPCLKOUT0_OUT           (tile0_gtpclkout0_i),
        .TILE0_GTPCLKOUT1_OUT           (tile0_gtpclkout1_i),
        //----------------- Transmit Ports - 8b10b Encoder Control -----------------
        .TILE0_TXCHARISK0_IN            (tile0_txcharisk0_i),
        .TILE0_TXCHARISK1_IN            (tile0_txcharisk1_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .TILE0_TXDATA0_IN               (tile0_txdata0_i),
        .TILE0_TXDATA1_IN               (tile0_txdata1_i),
        .TILE0_TXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE0_TXUSRCLK21_IN            (tile0_txusrclk0_i),
        //------------- Transmit Ports - TX Driver and OOB signalling --------------
        .TILE0_TXDIFFCTRL0_IN           (4'b0000),
        .TILE0_TXDIFFCTRL1_IN           (4'b0000),
        .TILE0_TXN0_OUT                 (TXN_OUT[0]),
        .TILE0_TXN1_OUT                 (TXN_OUT[1]),
        .TILE0_TXP0_OUT                 (TXP_OUT[0]),
        .TILE0_TXP1_OUT                 (TXP_OUT[1]),
        .TILE0_TXPREEMPHASIS0_IN        (3'b000),
        .TILE0_TXPREEMPHASIS1_IN        (3'b000),


        //_____________________________________________________________________
        //_____________________________________________________________________
        //TILE1  (X1_Y1)

        //---------------------- Loopback and Powerdown Ports ----------------------
        .TILE1_LOOPBACK0_IN             (3'b000),
        .TILE1_LOOPBACK1_IN             (3'b000),
        //------------------------------- PLL Ports --------------------------------
        .TILE1_CLK00_IN                 (tile0_gtp0_refclk_i),
        .TILE1_CLK01_IN                 (tile0_gtp0_refclk_i),
        .TILE1_GTPRESET0_IN             (tile1_gtpreset0_i),
        .TILE1_GTPRESET1_IN             (tile1_gtpreset1_i),
        .TILE1_PLLLKDET0_OUT            (tile1_plllkdet0_i),
        .TILE1_PLLLKDET1_OUT            (tile1_plllkdet1_i),
        .TILE1_RESETDONE0_OUT           (tile1_resetdone0_i),
        .TILE1_RESETDONE1_OUT           (tile1_resetdone1_i),
        //--------------------- Receive Ports - 8b10b Decoder ----------------------
        .TILE1_RXDISPERR0_OUT           (tile1_rxdisperr0_i),
        .TILE1_RXDISPERR1_OUT           (tile1_rxdisperr1_i),
        .TILE1_RXNOTINTABLE0_OUT        (tile1_rxnotintable0_i),
        .TILE1_RXNOTINTABLE1_OUT        (tile1_rxnotintable1_i),
        //------------- Receive Ports - Comma Detection and Alignment --------------
        .TILE1_RXENMCOMMAALIGN0_IN      (tile1_rxenmcommaalign0_i),
        .TILE1_RXENMCOMMAALIGN1_IN      (tile1_rxenmcommaalign1_i),
        .TILE1_RXENPCOMMAALIGN0_IN      (tile1_rxenpcommaalign0_i),
        .TILE1_RXENPCOMMAALIGN1_IN      (tile1_rxenpcommaalign1_i),
        //----------------- Receive Ports - RX Data Path interface -----------------
        .TILE1_RXDATA0_OUT              (tile1_rxdata0_i),
        .TILE1_RXDATA1_OUT              (tile1_rxdata1_i),
        .TILE1_RXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE1_RXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE1_RXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE1_RXUSRCLK21_IN            (tile0_txusrclk0_i),
        //----- Receive Ports - RX Driver,OOB signalling,Coupling and Eq.,CDR ------
        .TILE1_RXEQMIX0_IN              (2'b00),
        .TILE1_RXEQMIX1_IN              (2'b00),
        .TILE1_RXN0_IN                  (RXN_IN[2]),
        .TILE1_RXN1_IN                  (RXN_IN[3]),
        .TILE1_RXP0_IN                  (RXP_IN[2]),
        .TILE1_RXP1_IN                  (RXP_IN[3]),
        //------------- Receive Ports - RX Loss-of-sync State Machine --------------
        .TILE1_RXLOSSOFSYNC0_OUT        (tile1_rxlossofsync0_i),
        .TILE1_RXLOSSOFSYNC1_OUT        (tile1_rxlossofsync1_i),
        //-------------------------- TX/RX Datapath Ports --------------------------
        .TILE1_GTPCLKOUT0_OUT           (tile1_gtpclkout0_i),
        .TILE1_GTPCLKOUT1_OUT           (tile1_gtpclkout1_i),
        //----------------- Transmit Ports - 8b10b Encoder Control -----------------
        .TILE1_TXCHARISK0_IN            (tile1_txcharisk0_i),
        .TILE1_TXCHARISK1_IN            (tile1_txcharisk1_i),
        //---------------- Transmit Ports - TX Data Path interface -----------------
        .TILE1_TXDATA0_IN               (tile1_txdata0_i),
        .TILE1_TXDATA1_IN               (tile1_txdata1_i),
        .TILE1_TXUSRCLK0_IN             (tile0_txusrclk0_i),
        .TILE1_TXUSRCLK1_IN             (tile0_txusrclk0_i),
        .TILE1_TXUSRCLK20_IN            (tile0_txusrclk0_i),
        .TILE1_TXUSRCLK21_IN            (tile0_txusrclk0_i),
        //------------- Transmit Ports - TX Driver and OOB signalling --------------
        .TILE1_TXDIFFCTRL0_IN           (4'b0000),
        .TILE1_TXDIFFCTRL1_IN           (4'b0000),
        .TILE1_TXN0_OUT                 (TXN_OUT[2]),
        .TILE1_TXN1_OUT                 (TXN_OUT[3]),
        .TILE1_TXP0_OUT                 (TXP_OUT[2]),
        .TILE1_TXP1_OUT                 (TXP_OUT[3]),
        .TILE1_TXPREEMPHASIS0_IN        (3'b000),
        .TILE1_TXPREEMPHASIS1_IN        (3'b000)

    );
	 
	     //------------------------ User Module Resets -----------------------------
    // All the User Modules i.e. FRAME_GEN, FRAME_CHECK and the sync modules
    // are held in reset till the RESETDONE goes high. 
    // The RESETDONE is registered a couple of times on USRCLK2 and connected 
    // to the reset of the modules

    //--------------------------- Chipscope Connections -----------------------
    // When the example design is run in hardware, it uses chipscope to allow the
    // example design and GTP wrapper to be controlled and monitored. The 
    // EXAMPLE_USE_CHIPSCOPE parameter allows chipscope to be removed for simulation.
    

    // If Chipscope is not being used, drive GTP reset signal
    // from the top level ports
    assign  tile0_gtpreset0_i = !ALL_RESET_B;
    assign  tile0_gtpreset1_i = !ALL_RESET_B;
    assign  tile1_gtpreset0_i = !ALL_RESET_B;
    assign  tile1_gtpreset1_i = !ALL_RESET_B;


VMEEncoder VMEEncoder(.CLK(FPGA_GCLK), .RESET_B(ALL_RESET_B),
							 .CE_B(FPGA_SELECT), .WE_B(FPGA_WSTR_B), .OE_B(FPGA_RSTR_B),
							 .VME_A(VME_A), .VME_D(VME_D));

endmodule
