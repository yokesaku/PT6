`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:53:42 08/12/2015 
// Design Name: 
// Module Name:    glinkTX 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module glinkTX(
    input	wire  			CLK,
	 input	wire				RESET,
    input	wire	[15:0]  	in_data0,
    input	wire	[15:0]  	in_data1,
    input	wire	[15:0]  	in_data2,
	 input	wire	[15:0]  	in_data3,
    input	wire	[15:0]  	in_data4,
    input	wire	[15:0]  	in_data5,
    input	wire	[15:0]  	in_data6,
	 input	wire	[15:0]  	in_data7,
    input	wire	[15:0]  	in_data8,
    input	wire	[15:0]  	in_data9,
    input	wire	[15:0]  	in_dataa,
	 input	wire	[15:0]  	in_datab,
    input	wire	[15:0]  	in_datac,
    input	wire	[15:0]  	in_datad,
    input	wire	[15:0]  	in_datae,
	 input	wire	[15:0]  	in_dataf,
    input 	wire				in_flag0,
    input 	wire				in_flag1,
    input 	wire				in_flag2,
    input 	wire				in_flag3,
    input 	wire				in_flag4,
    input 	wire				in_flag5,
    input 	wire				in_flag6,
    input 	wire				in_flag7,
    input 	wire				in_flag8,
    input 	wire				in_flag9,
    input 	wire				in_flaga,
    input 	wire				in_flagb,
    input 	wire				in_flagc,
    input 	wire				in_flagd,
    input 	wire				in_flage,
    input 	wire				in_flagf,
	 input	wire	[1:0]		in_mode,
	 input	wire				in_locked, in_enabled,
	 input	wire	[1:0]		in_div,
	 input	wire				in_esmpxenb, in_flgenb, in_tclkenb, in_dis,
	 output	reg	[15:0]	out_data = 16'h0,
	 output	reg				out_flag = 1'b0,
	 output	reg	[1:0]		out_mode = 2'b01,
	 output	reg	[1:0]		out_div = 2'b01,
	 output	reg				out_flgenb = 1'b0, out_esmpxenb = 1'b0, out_tclkenb = 1'b0, out_dis = 1'b0
    );

wire	[15:0]	data[0:15];
assign  data[0] = in_data0;
assign  data[1] = in_data1;
assign  data[2] = in_data2;
assign  data[3] = in_data3;
assign  data[4] = in_data4;
assign  data[5] = in_data5;
assign  data[6] = in_data6;
assign  data[7] = in_data7;
assign  data[8] = in_data8;
assign  data[9] = in_data9;
assign data[10] = in_dataa;
assign data[11] = in_datab;
assign data[12] = in_datac;
assign data[13] = in_datad;
assign data[14] = in_datae;
assign data[15] = in_dataf;

wire				flag[0:15];
assign  flag[0] = in_flag0;
assign  flag[1] = in_flag1;
assign  flag[2] = in_flag2;
assign  flag[3] = in_flag3;
assign  flag[4] = in_flag4;
assign  flag[5] = in_flag5;
assign  flag[6] = in_flag6;
assign  flag[7] = in_flag7;
assign  flag[8] = in_flag8;
assign  flag[9] = in_flag9;
assign flag[10] = in_flaga;
assign flag[11] = in_flagb;
assign flag[12] = in_flagc;
assign flag[13] = in_flagd;
assign flag[14] = in_flage;
assign flag[15] = in_flagf;


reg	[3:0]	counter	=	4'h0;


always @(posedge CLK or negedge RESET) begin
	if ( !RESET | !in_locked ) begin 
		out_data  		<= 16'h0;
		out_flag			<= 1'b0;
		out_mode			<= 2'b01;
		out_div			<=	2'b01;
		out_flgenb		<= 1'b0;
		out_esmpxenb 	<= 1'b0;
		out_tclkenb 	<= 1'b0;
		out_dis			<= 1'b0;
	end
	else begin
		out_mode			<= in_mode;
		out_div			<= in_div;
		out_flgenb		<=	in_flgenb;
		out_esmpxenb	<=	in_esmpxenb;
		out_tclkenb		<=	in_tclkenb;
		out_dis			<=	in_dis;

		if ( in_enabled ) begin
			counter		=	counter + 4'b1;
			out_data 	<= data[counter];
			out_flag		<= flag[counter];
		end
		else begin
			out_data <= 16'h0;
			out_mode <= 2'b00;
		end
	end
end

endmodule
