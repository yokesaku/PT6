// Pt6Module.hh

#ifndef PT6MODULE_HH
#define PT6MODULE_HH

#include <stdint.h>
#include <sys/types.h>

class Pt6Module
{
public:
    enum result{ NO_ERROR,
                 INVALID_CHIP_ID,
                 INVALID_ADDRESS,
                 CANNOT_OPEN_FILE,
                 INVALID_FILE,
                 CONF_FAIL,
                 ERASE_FAIL };
    enum chip_id{ FPGA, CPLD, DPM };
    Pt6Module( off_t address );
    virtual ~Pt6Module( void );
    result writevme( chip_id chip, uint32_t address,
                    uint32_t value );
    result readvme( chip_id chip, uint32_t address,
                   uint32_t * value_p );
    result dump( const char * filename );
    result sdump( const char * filename );
//    result blank();
    result configure( const char * filename );
    result erase();

private:
    int32_t fd;
    uint32_t * base_address;
};

#endif
// PT6MODULE_HH
