#! /usr/bin/python

import struct
import sys
import binascii
import SSWclass
from SSWclass import SSWEvent
from SSWclass import SLBdata
import ROOT 
from ROOT import *
from array import array

argvs = sys.argv
argc = len(argvs)

def usage():
	print "usage): python ReadSSW.py [inputfile] [output dat file] [output root file]"
	exit(1)


def main():
	if argc != 4:
		usage()
	
	SSWout = argvs[1]
	Outfile = argvs[2]
	OutRoot = argvs[3]
	print "Inputfile: " + str(SSWout)
	print "Outputfile: " + str(Outfile)
	print "Output root file: " + str(OutRoot)
	fout = open(Outfile,'w')
	############## ROOT File ################
	ROOTout = TFile(OutRoot, "recreate")
	trout = TTree("SSWout","SSWout")
	
	v_SLBID = vector('int')(0)
	v_BCID = vector('int')(0)
	v_L1ID = vector('int')(0)
	v_RXID = vector('int')(0)
	v_datasize_C = vector('int')(0) # current data size
	v_datasize_P = vector('int')(0) # previous data size
	v_datasize_N = vector('int')(0) # next data size
	v_SLB_ovf_counter = vector('int')(0) 
	v_RX_ovf_counter = vector('int')(0) 
	v_RXError = vector('int')(0) 
	v_RXFifo = vector('int')(0) 
	v_SEU = vector('int')(0) 
	v_OVF = vector('int')(0) 
	v_LVDS = vector('int')(0) 
	
	i_eventNum = array('i',[0])
	i_SSWID = array('i',[0])
	
	l_vector = [v_SLBID, v_BCID, v_L1ID, v_RXID, v_datasize_C, v_datasize_P, v_datasize_N, v_SLB_ovf_counter, v_RX_ovf_counter, v_RXError, v_RXFifo, v_SEU, v_OVF, v_LVDS]
	
	trout.Branch('SLBID',v_SLBID)
	trout.Branch('BCID',v_BCID)
	trout.Branch('L1ID',v_L1ID)
	trout.Branch('RXID',v_RXID)
	trout.Branch('datasize_C',v_datasize_C)
	trout.Branch('datasize_P',v_datasize_P)
	trout.Branch('datasize_N',v_datasize_N)
	trout.Branch('SLB_ovf',v_SLB_ovf_counter)
	trout.Branch('RX_ovf',v_RX_ovf_counter)
	trout.Branch('RXError',v_RXError)
	trout.Branch('RXFifo',v_RXFifo)
	trout.Branch('SEU',v_SEU)
	trout.Branch('OVF',v_OVF)
	trout.Branch('LVDS',v_LVDS)
	trout.Branch('event',i_eventNum,'event/I')
	trout.Branch('SSWID',i_SSWID,'SSWID/I')
	################# ROOT File end #############
	eventnum = 0
	fin = open(SSWout, 'rb')
	indata = fin.read()

	eventstart = -1

	Read_EHeader = False
	isSLB = False
	Read_SLBHeader0 = False
	Read_SLBHeader1 = False
	inEvent = False
	mode = "None"
	PAD = 0x2000
	for ii in xrange(len(indata)/2):
		evenwords = []
		word =  int(binascii.hexlify(indata[2*ii]),16) + (int(binascii.hexlify(indata[2*ii+1]),16) << 8)
		
		if PAD == 0x2000 and (word >> 12) == 0x2:
			PAD = word + 1
			continue
		elif word == PAD and PAD == 0x21ff:
			PAD = 0x2100
			continue
		elif word == PAD:
			PAD = PAD + 1
			continue

		print "word=0x" + str(format(word,'X').zfill(4))

		if mode == "SLBHeader0":
			L1ID = (word >> 12) & 0xf
			BCID = word & 0xfff
			SLB.SetBCID(BCID)
			SLB.SetL1ID(L1ID)
			mode = "None"
			continue

		if mode == "SLBHeader1":
			SLB_OVF = (word >> 8) & 0xff
			RX_OVF = word & 0xff
			SLB.SetSLB_OVF(SLB_OVF)
			SLB.SetRX_OVF(RX_OVF)
			mode = "None"
			continue
		
		if mode=="EventHeader":
			mode = "None"
			continue

		if eventstart != 1 and word == 0x0b0f: #Event Start
			eventstart = 1
			event = SSWEvent(eventnum)
			print ""
			print "##### Event " + str(eventnum) + " ###########"
			Read_EHeader = False
			isSLB = False
			for vv in l_vector: # Initialize the root branches.
				vv.clear()
			continue

		if eventstart ==1 and word == 0x0b0f and inEvent: #Event start before End
			print "Error: New Event started before Event end"
			print "Event Number = " + str(eventnum)
			#exit(1)

		if word == 0x0e0f and eventstart == 1: #Event End
			trout.Fill()
			OutEvent(event, fout) # Write event to output file
			eventnum = eventnum + 1
			eventstart = -1
			continue

		if eventstart != 1: continue

		if not Read_EHeader and ((word) >> 11) == 0b001: # Event Header000
			isSLBHeader0 = False
			isSLBHeader1 = False
			SSWID = (word >> 7) & 0xf
			print "SSWID=" + str(SSWID)
			event.SetSSWID(SSWID)
			Read_EHeader = True
			mode="EventHeader"
			inEvent = True
			i_eventNum[0] = event.eventnum
			i_SSWID[0] = event.SSWID
			continue

		if Read_EHeader and (word >> 4) == 0xfca: # Event trailer
			inEvent = False
			if isSLB: 
				OutSLB(event, SLB, l_vector) # Write SLB ASIC to output file
				isSLB = False
			continue
		
		if not inEvent:  continue

		if (word >> 13) == 0b010: # SLB Header 010
			if isSLB: OutSLB(event, SLB, l_vector) # Write SLB ASIC to output file
			SLBID = (word >> 8) & 0x1f
			SLB = SLBdata(SLBID)
			mode = "SLBHeader0"
			isSLB = True
			isSLBHeader0 = True
			continue

		if (word >> 11) == 0b01100 and isSLB and isSLBHeader0: # SLB Header 011
			RXID = (word >> 6) & 0x1f
			RXFIFO = word & 0xf
			SLB.SetRXID(RXID)
			SLB.SetRXFIFO(RXFIFO)
			isSLBHeader1 = True
			mode = "SLBHeader1"
			continue

		if (word >> 13) == 0b100 and isSLB and isSLBHeader1: # SLB Current data
			cell = (word >> 8) & 0x1f
			bitmap = word & 0xff
			SLB.SetCurrent(cell, bitmap)
			continue

		if (word >> 13) == 0b101 and isSLB and isSLBHeader1: # SLB Previous data
			cell = (word >> 8) & 0x1f
			bitmap = word & 0xff
			SLB.SetPrevious(cell, bitmap)
			continue

		if (word >> 13) == 0b110 and isSLB and isSLBHeader1: # SLB Next data
			cell = (word >> 8) & 0x1f
			bitmap = word & 0xff
			if cell == 0x1f: continue # PAD word
			SLB.SetNext(cell, bitmap)
			continue
		
		if (word >> 12) == 0b0111 and isSLB and isSLBHeader1: # SLB trailer
			SEU = (word >> 11) & 1
			OVF = (word >> 10) & 1
			LVDS = (word >> 8) & 0x3
			RXERROR = word & 0xff
			SLB.SetSEU(SEU)
			SLB.SetOVF(OVF)
			SLB.SetLVDS(LVDS)
			SLB.SetRXERROR(RXERROR)
			OutSLB(event, SLB, l_vector)
			print "SLB contains error."
			print "event = " + str(eventnum)
			print "SLBID = " + str(SLB.SLBID)
			print "RXID = " + str(SLB.RXID)
			isSLB = False
			isSLBHeader0 = False
			isSLBHeader1 = False
			continue
			
	trout.Write()
	ROOTout.Close()
	fout.close()

def OutSLB(event, slb, l_vect): # Write out SLB ASIC
	#fout = open(outfile,'w')
	str_start = "##### New SLB Info Start. #########\n"
	str_ID = "SLBID = " + str(slb.SLBID) + ", RXID = " + str(slb.RXID) + ", BCID = " + str(slb.BCID) + ", L1ID = " + str(slb.L1ID) + "\n"
	str_ovf = "SLB overflow counter = " + str(slb.SLB_OVF) + ", RX overflow counter = " + str(slb.RX_OVF) + ", SLB overflow bit = " + str(slb.OVF) + ", SLB LVDS Link status = " + str(slb.LVDS) + ", SLB FIFO status = " + str(slb.RXFIFO) + ", SLB RXERROR status = " + hex(slb.RXERROR) + "\n"
	str_previous_head = "######## Previous hit map ############ \n"

	str_previous0 = str(format(slb.BitmapP[0] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapP[0] & 0x3,'b').zfill(2)) + str(format(slb.BitmapP[1],'b').zfill(8)) + str(format(slb.BitmapP[2],'b').zfill(8)) + str(format(slb.BitmapP[3],'b').zfill(8)) + str(format(slb.BitmapP[4] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapP[4] & 0x3,'b').zfill(2)) + str(format(slb.BitmapP[5] >> 4,'b').zfill(4)) + "\n"

	str_previous1 = str(format(slb.BitmapP[5] & 0xf,'b').zfill(4)) + str(format(slb.BitmapP[6] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapP[6] & 0x3f,'b').zfill(6)) + str(format(slb.BitmapP[7],'b').zfill(8)) + str(format(slb.BitmapP[8],'b').zfill(8)) + str(format(slb.BitmapP[9],'b').zfill(8)) + str(format(slb.BitmapP[10] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapP[10] & 0x3f,'b').zfill(6)) + "\n"

	str_previous2 = "    " + str(format(slb.BitmapP[11] >>6 ,'b').zfill(2)) + " " + str(format(slb.BitmapP[11] >> 0x3f,'b').zfill(6)) + str(format(slb.BitmapP[12],'b').zfill(8)) + str(format(slb.BitmapP[13],'b').zfill(8)) + str(format(slb.BitmapP[14] ,'b').zfill(8)) + str(format(slb.BitmapP[15] >> 6,'b').zfill(2)) + " " + str(format((slb.BitmapP[15] >> 4) & 0x3,'b').zfill(2)) + "\n"

	str_previous3 = "    " + str(format(slb.BitmapP[15] >>2 ,'b').zfill(2)) + " " + str(format(slb.BitmapP[15] >> 0x3,'b').zfill(2)) + str(format(slb.BitmapP[16],'b').zfill(8)) + str(format(slb.BitmapP[17],'b').zfill(8)) + str(format(slb.BitmapP[18] ,'b').zfill(8)) + str(format(slb.BitmapP[19] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapP[19] & 0x3,'b').zfill(2)) + "\n"

	str_current_head = "######## Current hit map ############ \n"

	str_current0 = str(format(slb.BitmapC[0] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapC[0] & 0x3,'b').zfill(2)) + str(format(slb.BitmapC[1],'b').zfill(8)) + str(format(slb.BitmapC[2],'b').zfill(8)) + str(format(slb.BitmapC[3],'b').zfill(8)) + str(format(slb.BitmapC[4] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapC[4] & 0x3,'b').zfill(2)) + str(format(slb.BitmapC[5] >> 4,'b').zfill(4)) + "\n"

	str_current1 = str(format(slb.BitmapC[5] & 0xf,'b').zfill(4)) + str(format(slb.BitmapC[6] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapC[6] & 0x3f,'b').zfill(6)) + str(format(slb.BitmapC[7],'b').zfill(8)) + str(format(slb.BitmapC[8],'b').zfill(8)) + str(format(slb.BitmapC[9],'b').zfill(8)) + str(format(slb.BitmapC[10] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapC[10] & 0x3f,'b').zfill(6)) + "\n"

	str_current2 = "    " + str(format(slb.BitmapC[11] >>6 ,'b').zfill(2)) + " " + str(format(slb.BitmapC[11] >> 0x3f,'b').zfill(6)) + str(format(slb.BitmapC[12],'b').zfill(8)) + str(format(slb.BitmapC[13],'b').zfill(8)) + str(format(slb.BitmapC[14] ,'b').zfill(8)) + str(format(slb.BitmapC[15] >> 6,'b').zfill(2)) + " " + str(format((slb.BitmapC[15] >> 4) & 0x3,'b').zfill(2)) + "\n"
	str_current3 = "    " + str(format(slb.BitmapC[15] >>2 ,'b').zfill(2)) + " " + str(format(slb.BitmapC[15] >> 0x3,'b').zfill(2)) + str(format(slb.BitmapC[16],'b').zfill(8)) + str(format(slb.BitmapC[17],'b').zfill(8)) + str(format(slb.BitmapC[18] ,'b').zfill(8)) + str(format(slb.BitmapC[19] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapC[19] & 0x3,'b').zfill(2)) + "\n"

	str_next_head = "######## Next hit map ############ \n"

	str_next0 = str(format(slb.BitmapN[0] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapN[0] & 0x3,'b').zfill(2)) + str(format(slb.BitmapN[1],'b').zfill(8)) + str(format(slb.BitmapN[2],'b').zfill(8)) + str(format(slb.BitmapN[3],'b').zfill(8)) + str(format(slb.BitmapN[4] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapN[4] & 0x3,'b').zfill(2)) + str(format(slb.BitmapN[5] >> 4,'b').zfill(4)) + "\n"

	str_next1 = str(format(slb.BitmapN[5] & 0xf,'b').zfill(4)) + str(format(slb.BitmapN[6] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapN[6] & 0x3f,'b').zfill(6)) + str(format(slb.BitmapN[7],'b').zfill(8)) + str(format(slb.BitmapN[8],'b').zfill(8)) + str(format(slb.BitmapN[9],'b').zfill(8)) + str(format(slb.BitmapN[10] >> 6,'b').zfill(2)) + " " + str(format(slb.BitmapN[10] & 0x3f,'b').zfill(6)) + "\n"

	str_next2 = "    " + str(format(slb.BitmapN[11] >>6 ,'b').zfill(2)) + " " + str(format(slb.BitmapN[11] >> 0x3f,'b').zfill(6)) + str(format(slb.BitmapN[12],'b').zfill(8)) + str(format(slb.BitmapN[13],'b').zfill(8)) + str(format(slb.BitmapN[14] ,'b').zfill(8)) + str(format(slb.BitmapN[15] >> 6,'b').zfill(2)) + " " + str(format((slb.BitmapN[15] >> 4) & 0x3,'b').zfill(2)) + "\n"
	str_next3 = "    " + str(format(slb.BitmapN[15] >>2 ,'b').zfill(2)) + " " + str(format(slb.BitmapN[15] >> 0x3,'b').zfill(2)) + str(format(slb.BitmapN[16],'b').zfill(8)) + str(format(slb.BitmapN[17],'b').zfill(8)) + str(format(slb.BitmapN[18] ,'b').zfill(8)) + str(format(slb.BitmapN[19] >> 2,'b').zfill(6)) + " " + str(format(slb.BitmapN[19] & 0x3,'b').zfill(2)) + "\n"

	str_emp = "\n"
	event.AddSLB(str_start)
	event.AddSLB(str_ID)
	event.AddSLB(str_ovf)
	event.AddSLB(str_previous_head)
	event.AddSLB(str_previous0)
	event.AddSLB(str_previous1)
	event.AddSLB(str_previous2)
	event.AddSLB(str_previous3)
	event.AddSLB(str_current_head)
	event.AddSLB(str_current0)
	event.AddSLB(str_current1)
	event.AddSLB(str_current2)
	event.AddSLB(str_current3)
	event.AddSLB(str_next_head)
	event.AddSLB(str_next0)
	event.AddSLB(str_next1)
	event.AddSLB(str_next2)
	event.AddSLB(str_next3)
	event.AddSLB("\n")
	#fout.close()
	l_vect[0].push_back(slb.SLBID)
	l_vect[1].push_back(slb.BCID)
	l_vect[2].push_back(slb.L1ID)
	l_vect[3].push_back(slb.RXID)
	l_vect[4].push_back(slb.datasizeC)
	l_vect[5].push_back(slb.datasizeN)
	l_vect[6].push_back(slb.datasizeP)
	l_vect[7].push_back(slb.SLB_OVF)
	l_vect[8].push_back(slb.RX_OVF)
	l_vect[9].push_back(slb.RXERROR)
	l_vect[10].push_back(slb.RXFIFO)
	l_vect[11].push_back(slb.SEU)
	l_vect[12].push_back(slb.OVF)
	l_vect[13].push_back(slb.LVDS)

def OutEvent(event, fout):
	str_start = "###### Event " +str(event.eventnum) + ". ################# \n"
	str_sswid = "SSW ID = " + str(event.SSWID) + "\n"
	
	fout.write("\n")
	fout.write("\n")
	fout.write(str_start)
	fout.write(str_sswid)
	fout.write("\n")
	fout.write(event.SLBevents)



main()
	
