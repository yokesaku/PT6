//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Aug 24 20:42:50 2015 by ROOT version 6.04/02
// from TTree SSWout/SSWout
// found on file: test.root
//////////////////////////////////////////////////////////

#ifndef SSWout_h
#define SSWout_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

class SSWout {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<int>     *SLBID;
   vector<int>     *BCID;
   vector<int>     *L1ID;
   vector<int>     *RXID;
   vector<int>     *datasize_C;
   vector<int>     *datasize_P;
   vector<int>     *datasize_N;
   vector<int>     *SLB_ovf;
   vector<int>     *RX_ovf;
   vector<int>     *RXError;
   vector<int>     *RXFifo;
   vector<int>     *SEU;
   vector<int>     *OVF;
   vector<int>     *LVDS;
   Int_t           event;
   Int_t           SSWID;

   // List of branches
   TBranch        *b_SLBID;   //!
   TBranch        *b_BCID;   //!
   TBranch        *b_L1ID;   //!
   TBranch        *b_RXID;   //!
   TBranch        *b_datasize_C;   //!
   TBranch        *b_datasize_P;   //!
   TBranch        *b_datasize_N;   //!
   TBranch        *b_SLB_ovf;   //!
   TBranch        *b_RX_ovf;   //!
   TBranch        *b_RXError;   //!
   TBranch        *b_RXFifo;   //!
   TBranch        *b_SEU;   //!
   TBranch        *b_OVF;   //!
   TBranch        *b_LVDS;   //!
   TBranch        *b_event;   //!
   TBranch        *b_SSWID;   //!

   SSWout(TTree *tree=0);
   virtual ~SSWout();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef SSWout_cxx
SSWout::SSWout(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("test.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("test.root");
      }
      f->GetObject("SSWout",tree);

   }
   Init(tree);
}

SSWout::~SSWout()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t SSWout::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t SSWout::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void SSWout::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   SLBID = 0;
   BCID = 0;
   L1ID = 0;
   RXID = 0;
   datasize_C = 0;
   datasize_P = 0;
   datasize_N = 0;
   SLB_ovf = 0;
   RX_ovf = 0;
   RXError = 0;
   RXFifo = 0;
   SEU = 0;
   OVF = 0;
   LVDS = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("SLBID", &SLBID, &b_SLBID);
   fChain->SetBranchAddress("BCID", &BCID, &b_BCID);
   fChain->SetBranchAddress("L1ID", &L1ID, &b_L1ID);
   fChain->SetBranchAddress("RXID", &RXID, &b_RXID);
   fChain->SetBranchAddress("datasize_C", &datasize_C, &b_datasize_C);
   fChain->SetBranchAddress("datasize_P", &datasize_P, &b_datasize_P);
   fChain->SetBranchAddress("datasize_N", &datasize_N, &b_datasize_N);
   fChain->SetBranchAddress("SLB_ovf", &SLB_ovf, &b_SLB_ovf);
   fChain->SetBranchAddress("RX_ovf", &RX_ovf, &b_RX_ovf);
   fChain->SetBranchAddress("RXError", &RXError, &b_RXError);
   fChain->SetBranchAddress("RXFifo", &RXFifo, &b_RXFifo);
   fChain->SetBranchAddress("SEU", &SEU, &b_SEU);
   fChain->SetBranchAddress("OVF", &OVF, &b_OVF);
   fChain->SetBranchAddress("LVDS", &LVDS, &b_LVDS);
   fChain->SetBranchAddress("event", &event, &b_event);
   fChain->SetBranchAddress("SSWID", &SSWID, &b_SSWID);
   Notify();
}

Bool_t SSWout::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void SSWout::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t SSWout::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef SSWout_cxx
