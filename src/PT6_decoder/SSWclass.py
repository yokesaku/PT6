class SSWEvent:
	def __init__(self,eventnum):
		#print "Event number = " + str(eventnum)
		self.eventnum = eventnum
		self.Mode = "None"
		self.SLBevents = ""
	
	def SetSSWID(self,sswid):
		self.SSWID = sswid
		#print "SSWID = " + str(self.SSWID)

	def SetMode(self, mode):
		self.Mode = mode # None, EventHeader, SLB header0, SLB header1, SLB trailer, SLB data C, SLB data P, SLB data N, Event Trailer

	def AddSLB(self, str): # Add SLB info
		self.SLBevents = self.SLBevents + str


class SLBdata:
	def __init__(self, slbid):
		self.SLBID = slbid
		self.BCID = -1
		self.L1ID = -1
		self.BitmapC = []
		self.BitmapP = []
		self.BitmapN = []
		self.datasizeC = 0
		self.datasizeP = 0
		self.datasizeN = 0
		self.SLB_OVF = 0
		self.RX_OVF = 0
		self.RXID = -1
		self.RXFIFO = 0
		self.SEU = 0
		self.OVF = 0
		self.LVDS = 0
		self.RXERROR = 0
		for ii in xrange(40):
			self.BitmapC.append(0)
			self.BitmapP.append(0)
			self.BitmapN.append(0)

	def SetSLBID(self, slbid): # SLBID
		self.SLBID = slbid
	
	
	def SetBCID(self, bcid): # BDID
		self.BCID = bcid
	
	def SetL1ID(self, l1id): # L1ID
		self.L1ID = l1id

	def SetSLB_OVF(self,slb_ovf): # SLB overflow counter (8 bit) 
		self.SLB_OVF = slb_ovf

	def SetRX_OVF(self,rx_ovf): # RX Overflow counter (8 bit)
		self.RX_OVF = rx_ovf

	def SetRXID(self,rxid): # RXID 
		self.RXID = rxid

	def SetRXFIFO(self,rxfifo): # RX FIFO status
		self.RXFIFO = rxfifo

	def SetCurrent(self, cell, data): # Current data 
		self.BitmapC[cell] = data
		self.datasizeC = self.datasizeC + 1
 
	def SetNext(self, cell, data): # Next data
		self.BitmapN[cell] = data
		self.datasizeN = self.datasizeN + 1

	def SetPrevious(self, cell, data): # Previous data
		self.BitmapP[cell] = data
		self.datasizeP = self.datasizeP + 1

	def SetSEU(self, seu): # SEU bit
		self.SEU = seu

	def SetOVF(self, ovf): # overflow bit
		self.OVF = ovf

	def SetLVDS(self, lvds): # LVDS Link status (2bits) {now, old} 1:Not linked. 0:Linked
		self.LVDS = lvds

	def SetRXERROR(self, rxerror): # RX ERROR state (8 bits)
		self.RXERROR = rxerror
	






