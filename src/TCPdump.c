//TCPdump.c 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <sys/socket.h>
#include <time.h>
#include <netdb.h>
//#include <netinet/in.h>

/*
#define TIME_SEC2NSEC 1000000000 
unsigned long long GetRealTimeInterval( const struct timespec *pFrom, const struct timespec *pTo ) {
	unsigned long long llStart = ( unsigned long long )( ( unsigned long long )pFrom -> tv_sec * TIME_SEC2NSEC
	+ ( unsigned long long )pFrom -> tv_nsec);
	unsigned long long llEnd = ( unsigned long long )( ( unsigned long long )pTo -> tv_sec * TIME_SEC2NSEC
	+ (unsigned long long )pTo -> tv_nsec);
	return( ( llEnd - llStart ) / 1000 );
}
*/
void show_usage( ) {
	printf("[usage]TCPdump [filename] [Server 0] [Server 1] ...\n");
	printf("[usage]argument 1  :filename w/o .xxx\n");
	printf("[usage]argument 2~ :lowest significant byte of IP addr\n");
	printf("[usage]15 servers allowed at most\n");
	printf("[usage]ex) TCPdump td 16 15\n");
}

int fd[16];
fd_set readfds;
int nRcv;
int nSel;
int nCon[16];
struct sockaddr_in sa[16];
struct timeval timeout;
//struct timespec tsStart, tsEnd;
FILE *datfile[16];
FILE *datfile_a[16];
FILE *datfile_b[16];
char filename[16][256];
char filename_a[16][256];
char filename_b[16][256];
char szBuf[1460];
//char szPort[256];
//char szServer[256];
unsigned long uport;
unsigned long uaddr[16];
unsigned long laddr[16] = {0};
unsigned long len[16] = {0};
unsigned long maxlen = 0;

int main( int argc, char* argv[] )
{
	if ( argc < 3 || argc > 17) {
		show_usage();
		return -1;
	}

	uport = 24;
	int i, j;
	for ( i = 0; i < ( argc - 2 ); i++ ) {
		laddr[i] = atol( argv[i+2] );
		if ( laddr[i] <= 0 || laddr[i] >= 255 ) {
			printf( "[error]server%d: address invalid\n", i  );
			printf( "[error]choose within 1 ~ 254\n", i  );
			return -2;
		}
		for ( j = 0; j < i; j++ ) {
			if ( laddr[j] == laddr[i] ) {
				printf( "[error]server%d: address equivalent to server %d\n", i, j );
				return -2;
			}
		}
		uaddr[i] = ( laddr[i] * 16777216 ) + ( 10 * 65536 ) + ( 168 * 256 ) + ( 192 * 1 );
		printf( "[info]server%d: IP address  = 192.168.10.%d\n", i, laddr[i] );
		printf( "[info]server%d: port number = %d\n", i, uport );
		
		strcpy( filename[i], argv[1] );
		strcat( filename[i], "_" );
		strcat( filename[i], argv[i+2] );
		datfile[i] = fopen( filename[i], "w" );
		if ( datfile[i] == NULL ) {
			printf( "[error]cannout open %s\n", filename[i] );
			return -2;
		}
	}

	for ( i = 0; i < ( argc - 2 ); i++ ) {
		fd[i] = socket( PF_INET, SOCK_STREAM, 0 );
		if ( fd[i] < 0 ) {
			printf( "[error]server%d: socket open error\n", i );
			return -3;
		} else {
			printf( "[info]server%d: socket open\n", i );
		}

		memset( &sa[i], 0, sizeof( sa[i] ) );
		sa[i].sin_family = AF_INET;
		sa[i].sin_port = htons( uport );
		sa[i].sin_addr.s_addr = uaddr[i];
	
		nCon[i] = connect( fd[i], ( struct sockaddr * ) &sa[i], sizeof( sa[i] ) );
		if ( nCon[i] == -1 ) {
			printf( "[error]server%d: connection error\n", i );
			return -3;
		} else {
			printf( "[info]server%d: connection established\n", i );
		}
	}
	
	FD_ZERO( &readfds );

//	clock_gettime( CLOCK_REALTIME, &tsStart );
	
	while ( maxlen < 0x40000 ) {
		for ( i = 0; i < ( argc - 2 ); i++ ) {
			FD_SET( fd[i], &readfds );
		}
		timeout.tv_sec = 0;
		timeout.tv_usec = 1000000;
		nSel = select( FD_SETSIZE, &readfds, 0, 0, &timeout );
		if ( nSel ) {
			for ( i = 0; i < ( argc - 2 ); i++ ) {
				if ( FD_ISSET( fd[i], &readfds ) ) {
					nRcv = recv( fd[i], szBuf, sizeof( szBuf ), 0 );
//					if ( nRcv == -1 ) {
//						printf( "[warning]server%d: receive error\n", i );
//						break;
//					}
					len[i] = len[i] + nRcv;
					if ( len[i] > maxlen ) {
						maxlen = len[i];
					}

					fwrite( &szBuf, sizeof( szBuf ), 1, datfile[i] );
				}
			}
		} else if ( nSel == 0 && maxlen > 1460 ) {
			printf( "[info]time out\n" );
			break;
		}
	}
	
//	clock_gettime( CLOCK_REALTIME, &tsEnd );
//	unsigned long long llusec = GetRealTimeInterval( &tsStart, &tsEnd );
//	printf( "read bytes %u / %llu = %gMbps\n", len, llusec, ( double )( len * 8 ) / llusec );

	for ( i = 0; i < ( argc - 2 ); i++ ) {
		printf( "[info]server%d: read %u bytes\n", i, len[i] );
		if ( nCon[i] != -1 ) {
			shutdown( fd[i], 2 );
			printf( "[info]server%d: shutdown\n", i );
		}
		close ( fd[i] );
		printf( "[info]server%d: socket closed\n", i );
		fclose( datfile[i] );
	}
	
	for ( i = 0; i < ( argc - 2 ); i++ ) {
		datfile[i] = fopen( filename[i], "rb" );
		
		strcpy( filename_a[i], filename[i] );
		strcat( filename_a[i], "a.dat" );
		datfile_a[i] = fopen( filename_a[i], "w" );
		if ( datfile_a[i] == NULL ) {
			printf( "[warning]cannout open %s\n", filename_a[i] );
			return -2;
		}
		strcpy( filename_b[i], filename[i] );
		strcat( filename_b[i], "b.dat" );
		datfile_b[i] = fopen( filename_b[i], "w" );
		if ( datfile_b[i] == NULL ) {
			printf( "[warning]cannout open %s\n", filename_b[i] );
			return -2;
		}

		char swapdata[2];
		while ( 1 ) {
			if ( fread( szBuf, 2, 1, datfile[i] ) == 1 ) {
				swapdata[1] = szBuf[0];
				swapdata[0] = szBuf[1]; 
				fwrite( &swapdata, 2, 1, datfile_a[i] );
			} else {
				break;
			}
			if ( fread( szBuf, 2, 1, datfile[i] ) == 1 ) {
				swapdata[1] = szBuf[0];
				swapdata[0] = szBuf[1]; 
				fwrite( &swapdata, 2, 1, datfile_b[i] );
			} else {
				break;
			}
		}
		fclose( datfile[i] );
		fclose( datfile_a[i] );
		fclose( datfile_b[i] );
	}
	
	return 0;
}

